/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dung_hashset;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.StringTokenizer;

/**
 *
 * @author Admin
 */
// chạy dữ liệu huấn luyện rồi ghi kết quả ra file
//ở class main khi dùng đến dữ liệu huấn luyện thì sẽ đọc lại từ file
public class run_data_huan_luyen {

    public static String readFile(String s) {
        try {
            Reader readerUnicode = new InputStreamReader(new FileInputStream(s), Charset.forName("UTF-16"));
            int e = 0;
            String kq = "";
            while ((e = readerUnicode.read()) != -1) {
// cast to char. The casting removes the left most bit. 
                if (e == 32 || e == 10 || e > 46) {
                    char f = (char) e;
                    String s1 = String.valueOf(f);
                    kq = kq + "" + s1;
                }
            }
            return kq;
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
        return null;
    }

    public static HashSet<String> bag_of_word(String s) {
        HashSet<String> bag = new HashSet<>();
        StringTokenizer s1 = new StringTokenizer(s, " ,.!*");// phân cách bằng các dấu ,.!*
        while (s1.hasMoreTokens()) {
            bag.add(s1.nextToken());
        }
        return bag;

    }

    public static void main(String[] args) throws IOException {

        ArrayList<HashSet<String>> arr_nonspam = new ArrayList<>();// mảng chứa các túi từ của thư nonspam
        ArrayList<HashSet<String>> arr_spam = new ArrayList<>();// mảng chứa các túi từ của thư spam
        ArrayList<String> x_i = new ArrayList<>(); // mảng chứa các thuộc tính x_i
        int sothurac = 10;
        int sothuthuong = 10;
        double P1 = (double) 0.5;
        double P2 = (double) 0.5;
        for (int i = 0; i < sothurac; i++) {
            String s1 = "non-spam/nonspam (" + i + ").txt";
            String s2 = "spam/spam (" + i + ").txt";
            String x1 = readFile(s1);
            arr_nonspam.add(bag_of_word(x1));
            String x2 = readFile(s2);
            arr_spam.add(bag_of_word(x2));
        }
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(new File("huanluyen.txt")));
        out.writeObject(arr_spam);
        out.writeObject(arr_nonspam);
        out.close();

    }
}
