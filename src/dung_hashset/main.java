/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dung_hashset;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Admin
 */
public class main extends javax.swing.JFrame {

    /**
     * Creates new form NewJFrame
     */
    static ArrayList<HashSet<String>> arr_nonspam = new ArrayList<>();// mảng chứa các túi từ của thư nonspam
    static ArrayList<HashSet<String>> arr_spam = new ArrayList<>();// mảng chứa các túi từ của thư spam
    static ArrayList<String> x_i = new ArrayList<>(); // mảng chứa các thuộc tính x_i
    static int sothurac = 10;
    static int sothuthuong = 10;
    static double P1 = (double) 0.5;
    static double P2 = (double) 0.5;
    

    public main() {
        initComponents();
        try {
            //đọc dữ liệu huấn luyện từ trước ở trong file huanluyen.txt ra
            ObjectInputStream inp= new ObjectInputStream(new FileInputStream(new File("huanluyen.txt")));
            arr_spam= (ArrayList<HashSet<String>>) inp.readObject();
            arr_nonspam= (ArrayList<HashSet<String>>) inp.readObject();
            inp.close();
        } catch (Exception ex) {
            System.out.println(ex.toString());
        } 
      
    }

    public static double P1(String x) { //tinh xac xuat P(xi=x|nhan= nonspam)
        double k=0;
        for(int i=0;i<arr_nonspam.size();i++){
            if(arr_nonspam.get(i).contains(x)) k++;  //moi lan x xuat hien trong 1 thu thuong thi k++
        }
        return (k+1)/(sothuthuong+1);
        //P(xi|nhan= nonspam)= (k+1)/(sothuthuong+1);
        //trong do: k la so cac mail nonspam xuat hien xi
        //sothuthuong la so mail nonspam
        
    }
      public static double P2(String x) { //tinh xac xuat P(xi=x|nhan= spam)
        double k=0;
        for(int i=0;i<arr_spam.size();i++){
            if(arr_spam.get(i).contains(x)) k++;//moi lan x xuat hien trong 1 thu rac thi k++
        }
        return (k+1)/(sothurac+1);
        //P(xi|nhan= spam)= (k+1)/(sothurac+1);
        //trong do: k la so cac mail spam xuat hien xi
        //sothurac la so mail spam
    }
     public static HashSet<String> bag_of_word(String s){
         HashSet<String> bag= new HashSet<>();
         StringTokenizer s1= new StringTokenizer(s," ,.!*");// phân cách bằng các dấu ,.!*
         while(s1.hasMoreTokens()){
             bag.add(s1.nextToken());
         }
         return bag;
            
        }
           
 
        public static String readFile(File file){
        try{
            Reader readerUnicode =new InputStreamReader(new FileInputStream(file), Charset.forName("UTF-16"));
        int e = 0;
        String kq="";
        while ((e = readerUnicode.read()) != -1) {
// cast to char. The casting removes the left most bit. 
            if(e==32||e==10||e>46){
                char f = (char) e;
            String s1= String.valueOf(f);
            kq=kq+""+s1;
            }
        }
        readerUnicode.close();
        return kq;
        }catch(Exception ex){
            System.out.println(ex.toString());
        }
        return null;
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButton1.setText("test");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        jTextArea1.setLineWrap(true);
        jTextArea1.setRows(5);
        jTextArea1.setWrapStyleWord(true);
        jScrollPane2.setViewportView(jTextArea1);

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jScrollPane3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jScrollPane3.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        jList1.setBackground(new java.awt.Color(236, 233, 216));
        jList1.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jList1ValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jList1);

        jScrollPane3.setViewportView(jScrollPane1);

        jMenu1.setText("File");

        jMenuItem1.setText("load from folder");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("open file");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItem4.setText("Exit");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem4);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 591, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed

        DefaultListModel listModel = new DefaultListModel();
        JFileChooser filechooser = new JFileChooser();
         filechooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                        filechooser.setAcceptAllFileFilterUsed(false);
                        filechooser.addChoosableFileFilter(new FileFilter() {
                            @Override
                            public boolean accept(File f) {
                                return f.isDirectory() || f.getName().toLowerCase().endsWith(".txt");
                            }

                            @Override
                            public String getDescription() {
                                return "Text Files (*.txt)";
                            }
                        });
        filechooser.showOpenDialog(null);
        File[] file = filechooser.getCurrentDirectory().listFiles();
        for (int i = 0; i < file.length; i++) {
            String s = file[i].getName();
            if (s.endsWith("." + "txt")) {
                System.out.println(s);
                element_jlist element = new element_jlist(file[i]);
                listModel.addElement(element);
            }
        }
        jList1.setModel(listModel);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed

          double C_NB1=P1; //P(xi|non-spam)
        double C_NB2=P2; //P(xi|spam)
        
           DefaultListModel listModel = new DefaultListModel();
        JFileChooser filechooser = new JFileChooser();
         filechooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                        filechooser.setAcceptAllFileFilterUsed(false);
                        filechooser.addChoosableFileFilter(new FileFilter() {
                            @Override
                            public boolean accept(File f) {
                                return f.isDirectory() || f.getName().toLowerCase().endsWith(".txt");
                            }

                            @Override
                            public String getDescription() {
                                return "Text Files (*.txt)";
                            }
                        });
        filechooser.showOpenDialog(null);
        File file= filechooser.getSelectedFile();
        System.out.println("tui tu test");
        String text= readFile(file);
        HashSet bag_test= bag_of_word(text);

        String [] arr_test= (String[]) bag_test.toArray(new String[bag_test.size()]);
//        String[] arr_test= bag_test.toArray(new String[bag_test.size()]);
        
        System.out.println("\n==========================");
        System.out.println("P(non-spam)= "+P1+"                "+"P(spam)= "+P2);
        
        for(int i=0;i<arr_test.length;i++){
            if(P1(arr_test[i])!=((double)1/(sothuthuong+1))||P2(arr_test[i])!=((double)1/(sothurac+1))){
                
//        System.out.println(P1(arr_test.get(i))+"   "+i+"       "+P2(arr_test.get(i)));
                System.out.println("P(x_i="+arr_test[i]+"|nonspam)=  "+P1(arr_test[i])+"        "+"P(x_i="+arr_test[i]+"|spam)=  "+P2(arr_test[i]));
            C_NB1*=P1(arr_test[i]);
            C_NB2*=P2(arr_test[i]);
            }
        }
        jTextArea1.setText(text);
        System.out.println("P1= "+C_NB1+"      P2= "+C_NB2);
        if(C_NB1<C_NB2){
            System.out.println("SPAM");
            jLabel1.setText("LÀ THƯ RÁC");
        }
            
        else{
            System.out.println("la thu thuong");
            jLabel1.setText("LÀ THƯ THƯỜNG");
        }
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        
        double C_NB1=P1; //P(xi|nonspam)
        double C_NB2=P2; //P(xi|spam)
        String text= jTextArea1.getText();
//        text= text.replaceAll(".", " ");
//        text= text.replaceAll(";", " ");
//        text= text.replaceAll("(", " ");
//        text= text.replaceAll(")", " ");
//        text= text.replaceAll("!", " ");
        if(text!=null){
            HashSet bag_test= bag_of_word(text);
         String [] arr_test= (String[]) bag_test.toArray(new String[bag_test.size()]);
        
        System.out.println("\n==========================");
        System.out.println("P(non-spam)= "+P1+"                "+"P(spam)= "+P2);
        
        for(int i=0;i<arr_test.length;i++){
            if(P1(arr_test[i])!=((double)1/(sothuthuong+1))||P2(arr_test[i])!=((double)1/(sothurac+1))){
                
//        System.out.println(P1(arr_test.get(i))+"   "+i+"       "+P2(arr_test.get(i)));
                System.out.println("P(x_i="+arr_test[i]+"|nonspam)=  "+P1(arr_test[i])+"        "+"P(x_i="+arr_test[i]+"|spam)=  "+P2(arr_test[i]));
            C_NB1*=P1(arr_test[i]);
            C_NB2*=P2(arr_test[i]);
            }
        }
        jTextArea1.setText(text);
        System.out.println("P1= "+C_NB1+"      P2= "+C_NB2);
        if(C_NB1<C_NB2){
            System.out.println("SPAM");
            jLabel1.setText("LÀ THƯ RÁC");
        }
            
        else{
            System.out.println("la thu thuong");
            jLabel1.setText("LÀ THƯ THƯỜNG");
        }
        }
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jList1ValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jList1ValueChanged

         double C_NB1=P1; //P(xi|non-spam)
        double C_NB2=P2; //P(xi|spam)
        System.out.println("tui tu test");
        element_jlist e= (element_jlist)jList1.getSelectedValue();
        File file= e.file;
        String text= readFile(file);
        HashSet bag_test= bag_of_word(text);
         String [] arr_test= (String[]) bag_test.toArray(new String[bag_test.size()]);
        
        System.out.println("\n==========================");
        System.out.println("P(non-spam)= "+P1+"                "+"P(spam)= "+P2);
        
        for(int i=0;i<arr_test.length;i++){
            if(P1(arr_test[i])!=((double)1/(sothuthuong+1))||P2(arr_test[i])!=((double)1/(sothurac+1))){
                
//        System.out.println(P1(arr_test.get(i))+"   "+i+"       "+P2(arr_test.get(i)));
                System.out.println("P(x_i="+arr_test[i]+"|nonspam)=  "+P1(arr_test[i])+"        "+"P(x_i="+arr_test[i]+"|spam)=  "+P2(arr_test[i]));
            C_NB1*=P1(arr_test[i]);
            C_NB2*=P2(arr_test[i]);
            }
        }
        jTextArea1.setText(text);
        System.out.println("P1= "+C_NB1+"      P2= "+C_NB2);
        if(C_NB1<C_NB2){
            System.out.println("SPAM");
            jLabel1.setText("LÀ THƯ RÁC");
        }
            
        else{
            System.out.println("la thu thuong");
            jLabel1.setText("LÀ THƯ THƯỜNG");
        }
            
    }//GEN-LAST:event_jList1ValueChanged

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed

        System.exit(0);
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
            //cai lai giao dien
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex) {
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new main().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JList jList1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextArea jTextArea1;
    // End of variables declaration//GEN-END:variables
}
